# ЗАДАНИЕ "ДОМАШНИ ЛЮБИМЦИ"

# Садържание

- [Инсталация](#markdown-header-_2)
    - [Настройка на базата](#markdown-header-_3)
    - [Настройка на проекта](#markdown-header-_4)
	    - [wizard.php](#markdown-header-_5)
	    - [ръчно](#markdown-header-_6)
- [Стартиране](#markdown-header-_7)
    - [wizard.php](#markdown-header-_8)
	- [ръчно](#markdown-header-_9)
- [Тестване](#markdown-header-_10)
- [Описание на решението](#markdown-header-_13)
	- [Frontend](#markdown-header-backend)
	- [Backend](#markdown-header-frontend)
	- [връзка м/у backend и frontend](#markdown-header-backend-frontend)
	
# Инсталация
### Настройка на базата
За да тръгнат нещата, са нужни следните неща : 

- PHP 7
- MySQL 
- Composer

Преди да опитате да инсталирате нещата, е необходимо да направите 2 бази данни. 
    
    database_name = petstore_uchase
    user_name = petstore
    password = petstore
    
    database_name = petstore_uchase_testing
      
 Ако искате да  ползвате други database user и password, редактирайте `petstore_api/.env`
### Настройка на проекта 
#### Автоматична (препоръчано)
 За да се натроят всички останали неща, е нужо да изпълните изпълнимия php файл wizard.php

    php wizard.php setup

 ИЛИ
 
#### Ръчна (НЕпрепоръчано)
 Инсталация на php package-ите + миграция на базата +  seed на базата с dummy редове
 
    cd petstore_api 
    composer install  
    php artisan migrate:refresh
    php artisan db:seed
    
   
# Стартиране на вече настроения проект

### Автоматично (препоръчано)

В различни shell/terminal прозореца 

    php wizard.php run backend 
И

    php wizard.php run frontend
    
### Ръчно  (НЕпрепоръчано)
 В различни shell/terminal прозореца 
 
    cd petstore_api/public
    php -S 127.0.0.1:8000 
 И
 
    cd petstore_frontend/public
    php -S localhost:5000
    
При успешно стартиране трябва да може да посетите [http://localhost:5000](http://localhost:5000) 
! Aко backend api-то не е пуснато, нещата няма да работят !

# Тестване 
#### Aко искате да изпълните unit тестовете 
### Автоматично (препоръчано)
    php wizard.php test
    
### Ръчно  (НЕпрепоръчано)
    cd petstore_api
    ./vendor/phpunit/phpunit/phpunit
    
    
# Описание на решението
### backend
Бизнес логиката е изградена с микро framework-а [Lumen](https://lumen.laravel.com/) ( базиран на [Laravel](https://laravel.com/) ). Избрах го, защото е лек и бърз, но в същото време мощен, почти колкото Laravel.
    
### frontend
* JS - Използван е [Svelte](https://svelte.dev/) - изключително бърз и мощен js framework, 
    който за разлика от [Vue](https://vuejs.org) и [React](https://reactjs.org) работи като компайлор до vanilajs.
* CSS - [Materialize](https://materializecss.com/)
### връзка м/у backend и frontend
Backend и frontend работят независимо. Backend-ът върши ролята на API с endpoint-ове, които може да разгледате [тук](https://documenter.getpostman.com/view/3505753/SzYaVHwe?version=latest).
В "приложението" има опция за login/регистрация (само log-нат user да може да създава любимец). Комуникацията между log-нат потребител и защитените endpoint-ове 
се извършва посредством [JsonWebToken (jwt)](https://jwt.io/)
     
     
# TL;DR
 
    php wizard.php setup
    php wizard.php run backend
    php wizard.php run frontend
    
    
# Поздрави :) 
### Нядявам се да нямате проблеми със стартирането и да ви допадне.   
    
 