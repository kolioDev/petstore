#!/bin/php
<?php

/*
    -------------Wizard.php ------------

    Малко фалче, което да помогне на вас
    да setup-не и run-нет моето решение
    на заданието "домашни любимци"
*/

if (count($argv) <2){
    echo "Please provide run option {run|setup|test}";
    exit(1);
}


$action = $argv[1];


switch ($action) {
//    Инсталация
    case "setup":
        echo "Setting the thngs up".PHP_EOL;
        echo "Executing composer instal".PHP_EOL;
        exec("cd petstore_api && composer install");
        echo PHP_EOL."Migrating the DB".PHP_EOL;
        exec("cd petstore_api && php artisan migrate:refresh");
        echo PHP_EOL."Seeding the DB".PHP_EOL;
        exec("cd petstore_api && php artisan db:seed");
        break;
//        Dev servere
    case "run":
        echo "running some cool stuff";
        if (count($argv) <3){
            echo "Expecting second arg {backend|frontend}".PHP_EOL;
            exit(1);
        }
        $runType = $argv[2];
        switch($runType){
            case "frontend":{
                echo PHP_EOL."Serving the frontend (localhost:5000)".PHP_EOL;
                exec("cd petstore_frontend/public && php  -S localhost:5000");
                echo PHP_EOL."You should be able to go to http://localhost:5000".PHP_EOL;
                break;
            }

            case "backend":{
                echo "Serving the api (127.0.0.1:8000)".PHP_EOL;
                exec("cd petstore_api/public && php -S 127.0.0.1:8000");
                echo PHP_EOL."Now open another terminal and serve the frontend".PHP_EOL;
                break;
            }

            default:
                echo "Expecting second arg {backend|frontend}".PHP_EOL;
                break;
        }

        break;
//        Тестове
    case "test":
        echo "testing some hopefully not bugy stuff...".PHP_EOL;
        exec("cd petstore_api && ./vendor/phpunit/phpunit/phpunit", $lines);
        foreach ($lines as $line) {
            echo $line.PHP_EOL   ;
        }
        break;
    default:
        echo "Please provide run option {run|setup|test}";
        break;
}

echo PHP_EOL;
