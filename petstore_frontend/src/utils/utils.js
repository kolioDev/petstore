//Връща първата валидационна грешка за дадено поле
export let getFirstError = (errors, key)=>{
    if (!errors) return false;
    if (!(key in errors)) return false;
    return errors[key]!==true ? errors[key][0]:true;
};

export default{}