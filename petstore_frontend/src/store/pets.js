/**
 *  Store за домашните любимци
 *
 */

import axios from 'axios'
import { writable } from 'svelte/store';
import { get } from 'svelte/store';
import { showOnPage } from "../globals";
import { getToken } from "./user";


const petsStore = writable([]); //домашните любимци
const petsCountStore = writable(0); //броя на всички домашни любимци, отоговарящи на критериите за търсене (не зависи от критериите за paginate-ване)
const searchParamsStore = writable(null); //критериите за търсене {name, type}


/**
 * Зарежда домашните любимци
 *
 * @param skip int? SQL offset
 * @param take int? SQL limit
 * @returns {Promise<void>}
 */
let load = async (skip = null, take = null) => {
    let responseData;
    let search = get(searchParamsStore);
    let name = null;
    let type = null;
    if (search) {
        name = search.name;
        type = search.type;
    }

    try {
        ({ data: responseData } = await axios.get("/pets/get", {
            params: {
                get: take, skip, name, type
            }
        }));
    } catch (e) {
        console.log(e)
    }

    petsStore.set(responseData['pets'])
    petsCountStore.set(responseData['count'])
};

/**
 * Прилага гритерии за тъсене
 *
 * @param name
 * @param type
 */
let filter = (name = null, type = null) => {
    let searchParams = {
        name, type
    };
    searchParamsStore.set(searchParams);
    load(null, showOnPage);
};

/**
 * Създава нов домашен любимец
 * @param name string
 * @param type int
 * @param about string
 * @returns {Promise<void>}
 */
let create = async (name, type, about) => {
    try {
        await axios.put('/pets/new', {
            name, type, about,
            _token:getToken()
        });

    } catch ({ response }) {
        throw response.data;
        return
    }
    load(null, showOnPage)
};

let subscribeToPets = petsStore.subscribe;
let subscribeToPetsCount = petsCountStore.subscribe;
let subscribeTosearchParams = searchParamsStore.subscribe;

export default {
    load, filter, create, subscribeToPets, subscribeToPetsCount, subscribeTosearchParams
}