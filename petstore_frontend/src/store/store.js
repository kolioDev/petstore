import axios from 'axios'
import { apiUrl } from "../globals";
import pets from "./pets"
import user from "./user"

// Настройки на axios http библиотеката
axios.defaults.baseURL = apiUrl;
axios.defaults.headers.common['Content-Type'] = 'application/json';


//Export-ва нужните моетоди

export let loadPets = pets.load;
export let filterPets = pets.filter;
export let createPet = pets.create;
export let subscribeToPets = pets.subscribeToPets;
export let subscribeToPetsCount = pets.subscribeToPetsCount;
export let subscribeTosearchParams = pets.subscribeTosearchParams;

export let userRegister = user.register;
export let userLogin = user.login;
export let subscribeToUser = user.subscribeToUser;
export let loadUser = user.loadUser;
export let logOutUser = user.logOut;