/**
 *  Store за домашните логнатия потребител
 *
 */

import axios from "axios";
import { writable } from 'svelte/store';
import Cookie from "js-cookie"
import { get } from 'svelte/store';

const userStore = writable(null); //държи логнатия потребител
const jwtStore = writable(null); //държи JWT

/**
 * Пробва да log-не или регистрира потребител
 *
 * @param type string = login|register
 * @param email string
 * @param password string
 * @param password_confirmation string?
 * @returns {Promise<*>}
 */
let auth = async (type, email, password, password_confirmation = null) => {
    let data;
    try {
        ({ data } = await axios.post(`/user/${type}`, {
            email, password, password_confirmation
        }));
    } catch ({ response }) {
        throw response.data
    }

    userStore.set(data.user);
    jwtStore.set(data.jwt);
    Cookie.set('api_jwt', data.jwt);

    return data
};

/**
 * Пробва да логне потребител
 *
 * @param email string
 * @param password string
 * @returns {Promise<*>}
 */
let login = (email, password) => {
    return auth('login', email, password)
};

/**
 * Пробва да регистрира потребител
 *
 * @param email string
 * @param password string
 * @param password_confirmation string
 * @returns {Promise<*>}
 */
let register = (email, password, password_confirmation) => {
    return auth('register', email, password, password_confirmation)
};

/**
 * Взима данните за потребител на база JWT
 *
 * @returns {Promise<*>}
 */
let loadUser = async () => {
    let jwt;
    if (!(jwt = get(jwtStore))) {
        jwt = Cookie.get('api_jwt');
        jwtStore.set(jwt)
    }

    let user;
    try {
        ({ data: user } = await axios.get('/user/', {
            params: {
                _token: jwt
            }
        }));
    } catch (e) {
        console.log(e)
    }
    userStore.set(user);
    return user;
};

/**
 * Log out-ва потребител
 * Занулява userStore и jwtStore
 * @returns {Promise<void>}
 */
let logOut = async()=>{
    userStore.set(null);
    jwtStore.set(null);
    Cookie.set('api_jwt', null);
};

export let getToken = ()=>{
    return get(jwtStore )
};

let subscribeToUser = userStore.subscribe;

export default {
    login, register, subscribeToUser, loadUser,logOut, 
}