export const apiUrl = 'http://127.0.0.1:8000/';
export const showOnPage = 7; //по колко домашни любимеца да зарежда на 1 станица
export const animals = [ //Видовете животни
    'куче',
    'котка',
    'морско свинче',
    'заек',
    'кон',
    'папагал',
    'костенурка',
    'прилеп',
    'змия',
    'гекон',
    'питон',
    'слон',
    'златна рибка',
    'сребърна рибка',
    'прилеп',
    'крава',
    'офца',
    'невестулка',
    'катерица',
    'хамстер',
    'морска звезда',
    'носорог',
    'мравка',
    'морско конче',
    'водно конче',
    'пони',
    'мишка',
    'плъх',
    'орел',
    'сова',
    'лешояд',
    'гълъб',
    'козел',
    'прасе',
    'рак',
    'омар',
    'алигатор',
    'хамелион',
    'тарантула',
    'мармот',
    'къртица',
    'сляпо прасе',
    'сурикат',
    'магаре',
    'жираф',
    'пингвин',
    'капибара',
    'хипопотам',
    'канарче', 
    'мишелов', 
    'сепия'
];

export default {
    apiUrl,
    animals,
    showOnPage,
};