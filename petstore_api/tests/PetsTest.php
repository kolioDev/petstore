<?php

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use \App\User;

class PetsTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * Тества създаването на домашен любимец
     *
     * @return void
     */
    public function testNew()
    {
        $data = [
            "about"=>"много обича да лае",
            "name"=>"гошо минор",
            "type"=>3,
        ];

        //# Всичко ок НО потребителят не е логнат
        $response = $this->call('put','/pets/new', $data);
        $this->assertEquals(401, $response->status());

        //# Всичко ОК
        $user = factory('App\User')->create();
        $data["_token"]= $this->getJWT($user->email);
        $response = $this->call('put','/pets/new', $data);
        $this->assertEquals(200, $response->status());

        //# Валидационни грешки
        $data = [
            "_token"=>$this->getJWT($user->email),
            "about"=>"крат",
            "name"=>"@",
            "type"=>-10,
        ];
        $response = $this->call('put','/pets/new', $data);
        $this->assertEquals(422, $response->status());
        $this->seeJson([
            'about'=>['Полето за описание трябва да е поне 10 символа'],
            'name'=>['Полето за име трябва да е поне 2 символа'],
            'type'=>['Невалиден тип']
        ]);
    }

    /**
     * Тества метода, който връща любимците
     *
     * @return void
     */
    public function testGetPets()
    {
        factory(App\User::class, 3)->create();
        factory('App\Pet',10)->create([
            'type'=>2,
        ]);

        factory('App\Pet',5)->create([
            'type'=>3,
        ]);

        factory('App\Pet',1)->create([
            'type'=>3,
            'name'=>"Гошо"
        ]);

        /**
         * прави завка проверява резултата
         *
         * @param string $params get параметри
         * @param int $received очаквания брой любимци
         * @param int $total очаквания общ брой любимци, отговарящи на search параметрите
         */
        $send = function($params,$received ,$total){
            $this->get('/pets/get?'.$params);
            $this->assertResponseOk();
            $data = json_decode($this->response->content());
            $this->assertCount($received, $data->pets);
            $this->assertEquals($total, $data->count);
        };

        //Всички домашни любимци
        $send('', 16,16);


        //С get и offset
        $send('get=5;skip=5;', 5,16);

        //Със search по тип
        $send('type=3', 6,6);

        //Със search по име
        $send('name=гошо', 1,1);

        //Със search по тип и име
        $send('name=гошо;type=2', 0,0);
    }


    /**
     * Връща jwt токен-а за някой user
     *
     * @return string
     */
    private function  getJWT($email){
        $user = User::where("email", $email)->first();
        return JWT::encode($user->api_token, env('APP_KEY'));
    }
}
