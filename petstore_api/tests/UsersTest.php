<?php

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use \App\User;

class UsersTest extends TestCase
{

    use DatabaseMigrations;
    /**
     * Тества регистрацията
     *
     * @return void
     */
    public function testRegister()
    {
        //# Всичко е ок
        $response = $this->call('post','/user/register', [
            'email' => 'test@test.test',
            'password'=> 'test_qwerty',
            'password_confirmation'=>'test_qwerty'
        ]);

        $this->assertEquals(200, $response->status());
        //Проверява, дали JWT-на е наред
        $this->checkJWT();

        //# Пробва да създаде втори user със същия email
        $response = $this->call('post','/user/register', [
            'email' => 'test@test.test',
            'password'=> 'test_qwerty',
            'password_confirmation'=>'test_qwerty'
        ]);
        $this->assertEquals(422, $response->status());
        $this->seeJson([
            'email'=>['Email-ът вече е зает'],
        ]);

        //# Празена post data
        $response = $this->call('post','/user/register', []);
        $this->assertEquals(422, $response->status());
        $this->seeJson([
            'email'=>['Моля въведете E-mail'],
            'password'=>['Моля въведете парола']
        ]);

        //№ невалидни полета
        $response = $this->call('post','/user/register', [
            'email' => str_pad('test', 125, 't', STR_PAD_LEFT),
            'password'=> 'qq',
            'password_confirmation'=>'test_qwerty'
        ]);
        $this->assertEquals(422, $response->status());
        $this->seeJson([
            'email'=>['Невалиден E-mail формат', 'Полето за E-mail не трябва да надхвърля 100 символа'],
            'password'=>['Полето за парола трябва да е поне 6 символа', 'Паролите не съвпадат']
        ]);
    }

    /**
     * Тества log-in a
     *
     * @return void
     */
    public function testLogin(){
        $pass="pa$$$$$$$";
        $user = factory('App\User')->make([
            'password'=>Hash::make( $pass)
        ]);
        $user->save();

        //# Всичко ок
        $response = $this->call('post','/user/login', [
            'email' =>  $user->email,
            'password'=> $pass,
        ]);
        $this->assertEquals(200, $response->status());
        $this->checkJWT( $user->email);

        //Невалиден email
        $response = $this->call('post','/user/login', [
            'email' =>  $user->email.'invalid',
            'password'=> $pass,
        ]);
        $this->assertEquals(403, $response->status());

        //Грешна парола
        $response = $this->call('post','/user/login', [
            'email' =>  $user->email,
            'password'=> $pass.'invalid',
        ]);
        $this->assertEquals(403, $response->status());

    }

    /**
     * Проверява, дали JWT токен-а е валиден
     *
     * @param $email string
     * @return void
     */
    private function checkJWT($email='test@test.test'){
        $user = User::where("email", $email)->first();
        $jwt = JWT::encode($user->api_token, env('APP_KEY'));
        $this->seeJson([
            'jwt'=>$jwt ,
        ]);
    }
}
