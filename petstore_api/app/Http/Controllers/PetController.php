<?php

namespace App\Http\Controllers;
use App\Pet;
use Illuminate\Http\Request;
use Laravel\Lumen\Http\ResponseFactory;

class PetController extends Controller
{

    private $pet;

    /**
     * Инициализира UserController
     *
     * @return void
     */
    public function __construct()
    {
        //Задава middleware
        $this->middleware('auth', ['only' => [
            'create',
        ]]);

        $this->pet = new Pet();
    }

    /**
     * Създава нов домашен любимеж
     *
     * @param  Request  $request
     * @throws
     * @return ResponseFactory
     */
    public function create(Request $request){
        $request['name'] = trim( $request->name);

        $this->validate($request, [
            'about'=> 'required|min:10|max:500',
            'type'=>'required|numeric|min:0|max:1000',
            'name'=>'required|min:2|max:50'
        ]);

        $pet = new Pet([
            'about'=>$request->about,
            'type'=>$request->type,
            'name'=>$request->name,
            'added_by'=>$request->user()->id,
        ]);

        $pet->save();

        return response($pet, 200);
    }

    /**
     * Връща домашните любимци
     * Може да прилага критерии за търсене и pagination
     *
     * @param  Request  $request
     * @throws
     * @return ResponseFactory
     */
    public function get(Request $request){
        $skip = $request->input("skip");
        $get = $request->input("get");
        $search=[
            'name'=>$request->input('name'),
            'type'=>$request->input('type'),
        ];

        $pets = $this->pet->getFiltered($skip, $get, $search);
        $petsCount = $this->pet->countFiltered($search);

        return response([
            'count'=>$petsCount,
            'pets'=>$pets,
        ], 200);
    }
}
