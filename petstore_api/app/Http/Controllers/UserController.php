<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use \Firebase\JWT\JWT;
use Laravel\Lumen\Http\ResponseFactory;

class UserController extends Controller
{
    /**
     * Регистрира потребител
     *
     * @param  Request  $request
     * @throws
     * @return ResponseFactory
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'email'=>'required|email|max:100|unique:users,email',
            'password'=>'required|max:100|min:6|confirmed',
            'password_confirmation'=>'required|same:password',
        ]);
        $email = $request->input('email');
        $pass =  Hash::make($request->input('password'));
        $token = Str::random(20);

        $user = new User(['email'=>$email]);
        $user->password =  $pass;
        $user->api_token = $token;
        $user->save();

        $jwt = JWT::encode($token, env('APP_KEY'));

        return response(['jwt'=>$jwt, 'user'=>$user], 200);
    }


    /**
     * Лог-ва потребител
     *
     * @param  Request  $request
     * @throws
     * @return ResponseFactory
     */
    public function login (Request $request)
    {
        $user= User::where("email",$request->input("email"))->first();

        if($user && Hash::check($request->input("password"),$user->password) ) {
            $jwt=JWT::encode($user->api_token , env('APP_KEY'));
            return response(['jwt'=>$jwt, 'user'=>$user], 200);
        }
        return   response('invalid credentials',403);
    }
}
