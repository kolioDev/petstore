<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract
{
    use Authenticatable;


    /**
     * Полетата, които могат да се попълват
     * при construct-ване на App/User
     *
     * @var array
     */
    protected $fillable = [
         'email',
    ];

    /**
     * Полетата, които да скрие от
     * json-а репрезентацията
     *
     * @var array
     */
    protected $hidden = [
        'password','api_token'
    ];
}
