<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Pet extends Model
{
    /**
     * Полетата, които могат да се попълват
     * при construct-ване на App/Pet
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'about',
        'type',
        'added_by',
    ];

    /**
     * Връща потребителя, който е добавил любимеца
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'added_by');
    }

    /**
     * Връща домашните любимци от базата
     * Които отговарят на критерите
     *
     * @param string $skip sql OFFSET
     * @param string $get  sql LIMIT
     * @param array $search масив с критерии за търсене (ключове type и name)
     * @return Collection
     */
    public function getFiltered($skip=null, $get=null, $search=null){
        $pets =$this->withSearch($search);
        if ($skip!=null){
            $pets = $pets->skip($skip);
        }
        if ($get!=null){
            $pets = $pets->take($get);
        }

        return $pets->orderBy('created_at', 'desc')->get()->map(function ($pet) {
            $pet->added_by = $pet->user->email;
            return $pet;
        });
    }

    /**
     * Връщя общия брой на записите, които отговарят на няква заявка за търсене
     *
     * @param array $search масив с критерии за търсене (ключове type и name)
     * @return int
     */
    public function countFiltered($search=null){
        return $this->withSearch($search)->count();
    }


    /**
     * Прилага scope за търсенето.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  array  $search  масив с критерии за търсене (ключове type и name)
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithSearch($query, $search)
    {
        if ($search!=null){
            if ( $name = $search['name']){
                $query =   $query->where('name', 'like', "%$name%");
            }
            if ($type = $search['type']){
                $query = $query->where('type', $type);
            }
        }
        return $query;
    }

}
