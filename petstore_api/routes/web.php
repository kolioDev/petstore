<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    return redirect(env('API_DOCS'));
});

$router->group(['prefix' => '/user'], function () use ($router) {
    //Регистрация на потребител
    $router->post('/register', 'UserController@register');
    //Логин на потребител
    $router->post('/login', 'UserController@login');
    //Връща логнатия user
    $router->get('/', function (Request $request) {
        return $request->user();
    });
});

$router->group(['prefix' => '/pets'], function () use ($router) {
    //Създаване на домашен любимец
    $router->put('/new', 'PetController@create');
    //Връща домашните любимци
    $router->get("/get", 'PetController@get');
});



