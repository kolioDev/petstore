<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Pet;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'api_token'=> Str::random(20),
        'password'=> Hash::make($faker->password),
    ];
});


$factory->define(Pet::class, function (Faker $faker) {
    return [
        'about'=>$faker->realText(340),
        'name'=>$faker->firstName,
        'type'=>random_int(0,50),
        'added_by'=>random_int(1,3),
    ];
});
