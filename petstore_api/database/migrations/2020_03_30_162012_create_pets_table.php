<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->id();
            $table->string("name", 50)->comment("името на домашния любимец");
            $table->string("about", 500)->comment("информация за домашният любимец");
            $table->smallInteger('type')->comment("видът на домашният любимец");
            $table->smallInteger("added_by")->comment("кой потребител го е добавил");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
