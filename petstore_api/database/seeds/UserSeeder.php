<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Seed-ва базата с user-и
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 3)->create();
    }
}
