<?php

use Illuminate\Database\Seeder;

class PetSeeder extends Seeder
{
    /**
     * Seed-ва базата с дошани любимци
     *
     * @return void
     */
    public function run()
    {
        factory(App\Pet::class, 60)->create();
    }
}
