<?php

return [
    'custom' => [
        'email' => [
            'unique' => 'Email-ът вече е зает',
        ],
        'password'=>[
            'confirmed'=>'Паролите не съвпадат',
        ],
        'password_confirmation'=>[
            'same'=>'Паролите не съвпадат',
        ],
        'type'=>[
            'min'=>'Невалиден тип',
            'max'=>'Невалиден тип'
        ]
    ],
    'required'=>'Моля въведете :attribute',
    'min.string'=>'Полето за :attribute трябва да е поне :min символа',
    'max.string'=>'Полето за :attribute не трябва да надхвърля :max символа',
    'email'=>'Невалиден E-mail формат',
    'alpha'=>'Полето за :attribute може да садържа само букви',

    'attributes' => [
        'email' => 'E-mail',
        'password'=>'парола',
        'password_confirmation'=>'потвърждение на паролата',
        'about'=>'описание',
        'type'=>'тип',
        'name'=>'име',
    ],
];
